

/**
 * Created by Александр on 06.03.2017.
 */
public class Contact {
    private String name, phone, mail;
//___________________________________________________________
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
//_____________________________________________________________
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
//_____________________________________________________________
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
//_____________________________________________________________
public String toString() {
    return "\tName: " + name +";\n\tPhone: " + phone + ";\n\tMail: " + mail;
}

}

