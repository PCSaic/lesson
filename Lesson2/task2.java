import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

class task2 {
    public static void main(String args []) throws IOException {
        int n;
        int i;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the number of strings: \n");
        n = Integer.parseInt(reader.readLine()) ;

        if (0 < n && n <= 100) {
            //------------
            String [] currentString = new String[n]; //create a string array
            for ( i = 0; i < n; i++) {                       // fill array
                System.out.print((i + 1) + ": ");            //*
                currentString[i] = reader.readLine();        //*
                System.out.println();                        //*
            }

            Arrays.sort (currentString);                      //alphabet check
            for (i=0;i<n;i++){
             for (int j = (n-1); j > i; j--) {
                 if (currentString[j].length() < currentString[j - 1].length()) {
                     String t = currentString[j];
                     currentString[j] = currentString[j - 1];
                     currentString[j - 1] = t;
                 }
             }
          }
          //----------------------------------------------------

                for (String s:  currentString)   // foreach =)
                     System.out.printf("MIN (%d): \"%s\"%n", s.length(), s);

        }else
            System.out.println("Input erorr, please check the entered number (0 < n <= 100)");
    }
}