import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Str {
    public static void main(String args []) throws IOException {
        int n;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number of strings:");
        n =Integer.parseInt(reader.readLine()) ;

        if (0 < n && n <= 100) {
            //------------
            String veryShortString = null;//
            String veryLongString = null;//
            for (int i = 0; i < n; i++) {
                System.out.print((i + 1) + ": ");
                String currentString = reader.readLine();//reads the string with all characters and spaces
                System.out.println();

                if (veryShortString == null || currentString.length() <= veryShortString.length()) {//comparison of short strings with current
                    veryShortString = currentString;// if true, the short string to assign the current string
                }
                if (veryLongString == null || currentString.length() >= veryLongString.length()) {//comparison of long strings with current
                    veryLongString = currentString;// if true, the long string to assign the current string
                }
            }
                //------------
                System.out.printf("MIN (%d): \"%s\"%n", veryShortString.length(), veryShortString);
                System.out.printf("MAX (%d): \"%s\"%n", veryLongString.length(), veryLongString);
                //------------

        }else
            System.out.println("Input erorr, please check the entered number (0 < n <= 100)");
    }
}