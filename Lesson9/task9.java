import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class task9{
    public static void main(String [] args) throws IOException{
        int n, sum = 0;
        //keyboard input
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number of the month: ");
        n = Integer.parseInt(reader.readLine());
        //the output of the matrix numbers
        for (int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++){
               sum++;
               System.out.print(sum + "\t");
            }
            //every "n" new string
            System.out.println();
        }
    }
}