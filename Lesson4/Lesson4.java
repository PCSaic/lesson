import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


class Lesson4{

public static void main(String args[] ) throws IOException{
        int n, i, iMin, iMinLength;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the number of strings: \n");
        n = Integer.parseInt(reader.readLine());

        String text = reader.readLine(); // Enter string

        String arr[]=text.split("[^a-zA-Z0-9]"); //create array of words from a entered string

    iMin = 0;
    iMinLength = arr[0].length();

        one:{  //label when you run a breack
            for (i=0; i < arr.length; i++) {
                if (arr[i].length() < iMinLength && i < n) {
                 iMinLength = arr[i].length();
                 iMin = i;
                }else if(n < i) break one; //if the number of words more n output error
            }
        }
    if (n < i) System.out.println("Input error");  // if one - true
    else System.out.println( "\n" + arr[iMin] );   //output the short word
}
}