package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class task7 {

    public static void main(String[] args) throws IOException{
        int n,i;
// Keyboard input______________________________________________________________________
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the number of strings: \n");
        n = Integer.parseInt(reader.readLine()) ;

        String text = reader.readLine();
        String [] arr = text.split("[^a-zA-Z]") ;// creat array of words
//______________________________________________________________________________________

        if (arr.length == n){ // check the number of entered words
            String[] a = new String[n];// creat new empty array

            for (i = 0; i < n; i++) {
                int r = 0;
                for (int j = 0; j <= i; j++) {
                    if (Objects.equals(arr[i], a[j])) {
                        r++;
                    }
                }
                if (r == 0) {
                    a[i] = arr[i]; //fill array with no repeated words
                }
            }
            for (i = 0; i < a.length;i++){
                if (a[i] == null){
                    a[i] = "";} //fill in the null position
            }

            for (String s : a) {// check each word for the presence of repetitions of symbols
                char[] ch = s.toCharArray();
                boolean f = false;
                for (char c : ch) {
                    int z = 0;
                    for (i = 0; i < s.length(); i++) {
                        if (c == ch[s.length() - (i+1)])
                            z++;
                    }
                    if (z > 1)
                        f = true;
                }
                if (f == false)
                    System.out.printf("%s ", s);// output sorted array
            }

        }
        else
            System.out.printf("Incorrect input!");// write your code here
    }
}

