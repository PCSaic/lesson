import java.io.BufferedReader;
import java.util.*;
import java.io.InputStreamReader;
import java.io.IOException;

public class task12{
    public static void main (String [] args) throws IOException {
        // Keyboard input
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.printf("Enter matrix size: %n");
        // Enter size of matrix
        int n = Integer.parseInt(reader.readLine());
        System.out.printf("Enter column: %n");
        //Enter the column which will be sorted
        int v = Integer.parseInt(reader.readLine());
        v--;
        //Create matrix n x n
        int Arr[][] = new int[n][n];
        //Fill the matrix with random numbers
        for (int i = 0; i<n; i++)
        {
            for (int j = 0; j < n; j++){
                Arr[i][j] = (int) (Math.random()*10);
            }
        }
System.out.println();

        int V = v;
        //Sort array, use L-expression
        Arrays.sort(Arr, (x, y) -> x[V]-y[V]);
        // Output sorted array
        for (int i = 0; i<n; i++)
        {
            for (int j = 0; j < n; j++){
               System.out.printf("%s   ", Arr[i][j]);
            }
           System.out.println();
        }
    }
}