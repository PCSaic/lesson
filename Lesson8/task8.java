import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class task8 {
    public static void main (String [] args) throws IOException{
        int n;
        //keyboard input:
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number of strings: ");
        n = Integer.parseInt(reader.readLine());
        String text = reader.readLine();
        String [] stringArr = text.split("[^a-zA-Z0-9]");
        //check the length of the array, if length = n - execute the program
        if (stringArr.length == n){
        int counter = 0;
        String x = null;
        for (String s : stringArr) {
            //check each length of word, if %2 != 0 - next s
            if (s.length() % 2 == 0) {
                int z = s.length() / 2;
                //creat char array
                char[] ch = s.toCharArray();
                int count = 0;
                for (int i = 0; i < z; i++) {
                    //looking for polynomials consisting of numbers
                    if (ch[i] == ch[(ch.length - i) - 1] && '0' <= ch[i] && ch[i] <= '9')
                        count++;
                }
                //consider the number of polynomials
                if (count == s.length() / 2){
                    counter++;
                    //alternately write the polynomial
                   if(counter <= 2)
                   x = s;
                   //if find second polynomial break loop
                   if (counter ==2)
                       break;
                }
            }
        }
        //check how array is filled
        if (x == null){
            System.out.printf("NOT FOUND!!! :(");
        }else
        System.out.printf("%s %n", x);
        }
        // if length array != n output: INPUT ERROR!
        else System.out.printf("INPUT ERROR!");
    }
}
