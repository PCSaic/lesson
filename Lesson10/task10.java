import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class task10{
    public static void main (String [] args) throws IOException{
        double a,b,c,x1,x2,d;
        Scanner in = new Scanner(System.in);
        System.out.print("a = ");
        a = in.nextDouble();
        System.out.print("b = ");
        b = in.nextDouble();
        System.out.print("c = ");
        c = in.nextDouble();
        d = b*b - 4*a*c;
    if (a != 0 && d >= 0){
    x1 = ((-b)- Math.sqrt(d))/(2*a);
    x2 = ((-b)+ Math.sqrt(d))/(2*a);
    x1 = Math.round(x1*100);
    x2 = Math.round(x2*100);

    if (x1 == x2){
        System.out.printf("One solution: %n");
        System.out.printf("%s ",x1/100);
    }

    if (x1 != x2){
        System.out.printf("Two solution: %n");
        System.out.printf("%s  ",x1/100);
        System.out.printf("%s  ",x2/100);
    }
    }else
        System.out.print("No solution");
    }
}
