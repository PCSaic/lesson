package com.company;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class task6 {

	public static void main(String[] args) throws IOException{

// Keyboard input______________________________________________________________________

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the string:");
		String text = reader.readLine();
		String [] arr = text.split("[^a-zA-Z]") ;
//_______________________________________________________________________________________

		boolean b = true;
		for (String s: arr){
			char [] ch = s.toCharArray();
			int counter = 0;
			for (int i = 0; i < s.length()-1; i++){
				if (ch[i]<ch[i+1])                   //sort char
					counter++;
			}
			if (s.length()-counter - 1 == 0) {
				System.out.printf("%s ",s); //if the number of different symbols equal to the length of the word
				b = false;                    //output the word
			}

		}
		if (b == true)
			System.out.print("NOT FOUND!!!");// if the condition is false output NOT FOUND!!!
	}
}
