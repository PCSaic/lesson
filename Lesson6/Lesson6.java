import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Lesson6 {
	public static void main(String [] args) throws IOException {
		int co, i, j, n;
		boolean b = true;

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of strings: \n");
		n = Integer.parseInt(reader.readLine());
		String text = reader.readLine();

		String word[]=text.split("[^a-zA-Z0-9]");

     	if (n > word.length-1) { 									//Check the number of entered words
	    	for (j = 0; j < word.length; j++) {                     //Caled every word from the array

		    	char[] arrayChar = word[j].toCharArray();           //Convert the word into a character array
		    	for (i = arrayChar.length - 1; i > 0; i--) {
			    	b = true;
			    	co = (int) arrayChar[i] - (int) arrayChar[i - 1];//Check the order of characters in the word
			    	if (co < 0) {
				    	b = false;
				    	break;                                       //If the order of the characters wrong end
			    	}
		    	}
		    	if (b == true) {
			    	System.out.println(word[j]);    				//Show a word if the order is correct
			    	break;											//End programm
		    	}
	    	}
	    	if (b == false)
		    	System.out.println("NOT FOUND!");					//Show "NOT FOUND" if the order is not correct
       	}else System.out.println("INPUT ERORR!!!!!!");
	}
}
