import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class task3 {
    public static void main(String args []) throws IOException {
        int n;
        int i;
        int z = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the number of strings: \n");
        n = Integer.parseInt(reader.readLine()) ;

        String [] arr = new String[n]; //create a string array
            for ( i = 0; i < n; i++) {                       // fill array
                System.out.print((i + 1) + ": ");            //*
                arr[i] = reader.readLine();                  //*
            }
        for (String s: arr)   //find the average value
         z = z + s.length();
         z = z / n;
        System.out.printf("AVERAGE (%d) %n",z);

        for(String s: arr){ //compare the values with average
            if (s.length() < z) {
                    System.out.printf("(%d): \"%s\"%n", s.length(), s); //output values less than the average
            }
        }
    }
}
