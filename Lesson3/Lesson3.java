import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Array3 {
    public static void main(String args []) throws IOException {
        int n;
        int i;
        int z;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the number of strings: \n");
        n = Integer.parseInt(reader.readLine()) ;

        String [] arr = new String[n]; //create a string array
            for ( i = 0; i < n; i++) {                       // fill array
                System.out.print((i + 1) + ": ");            //*
                arr[i] = reader.readLine();                  //*
            }
        //find the average value---------------------------------------
        z = 0;
        for (i=0;i<n;i++)
            z = z + arr[i].length();
            z = z / n;
        //-------------------------------------------------------------
        System.out.printf("AVERAGE (%d) %n",z);

        for(i = 0; i < n; i++){ //compare the values with average
            if (arr[i].length() < z) {
                    System.out.printf("(%d): \"%s\"%n", arr[i].length(), arr[i]); //output values less than the average
            }
        }
    }
}
